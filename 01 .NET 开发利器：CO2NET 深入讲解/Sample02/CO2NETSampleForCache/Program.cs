﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Senparc.CO2NET;
using Senparc.CO2NET.Cache;
using Senparc.CO2NET.RegisterServices;

Console.WriteLine("Hello, World!");

var configBuilder = new ConfigurationBuilder();
var config = configBuilder.Build();

var services = new ServiceCollection();
services.AddMemoryCache();//添加内存缓存

//添加Senparc.CO2NET全局注册
services.AddSenparcGlobalServices(config);
Console.WriteLine("完成 CO2NET 全局注册");

var senparcSetting = new SenparcSetting()
{
    IsDebug = false,
    DefaultCacheNamespace = "DefaulCO2NETCache",
    Cache_Redis_Configuration = "localhost:6379"
};

//Start CO2NET
var register = RegisterService.Start(senparcSetting)
                              .UseSenparcGlobal();
register.ChangeDefaultCacheNamespace("SundaySharing20221211");

//完成Redis配置
Senparc.CO2NET.Cache.CsRedis.Register.SetConfigurationOption(senparcSetting.Cache_Redis_Configuration);

var serviceProvider = services.BuildServiceProvider();

var cache = CacheStrategyFactory.GetObjectCacheStrategyInstance();
Console.WriteLine("当前缓存策略：" + cache.GetType().Name);
Console.WriteLine("当前A的值："+ cache.Get("A"));

var input = int.Parse(Console.ReadLine());
cache.Set("A", input);

var readData = cache.Get<int>("A");
Console.WriteLine("您输入的值是：" + readData);

readData = readData + 1;
cache.Set("A", readData);

var newReadData = cache.Get("A");
Console.WriteLine("A的值已经被修改为：" + newReadData);

cache.RemoveFromCache("A");

var readData2 = cache.Get("A");
Console.WriteLine("A的值是：" + readData2);
Console.WriteLine("A为null：" + (readData2 == null));

Console.WriteLine();
Console.WriteLine("开始 Redis 测试");
Console.WriteLine("===============");

Senparc.CO2NET.Cache.CsRedis.Register.UseKeyValueRedisNow();

var redisCache = CacheStrategyFactory.GetObjectCacheStrategyInstance();
Console.WriteLine("当前缓存策略：" + redisCache.GetType().Name);
Console.WriteLine("当前B的值：" + redisCache.Get("B"));

redisCache.Set("B", input);
var redisValue = redisCache.Get<int>("B");
Console.WriteLine("Redis B："+ redisValue);
Console.WriteLine("B类型：" + redisValue.GetType().Name);

redisValue++;
redisCache.Set("B", redisValue, TimeSpan.FromSeconds(10));
var newRedisValue = redisCache.Get<int>("B");
Console.WriteLine("Redis B：" + newRedisValue);

Console.ReadKey();