﻿using Senparc.Ncf.Service;
using System;

namespace Sunday.Xncf.DemoTest.Areas.DemoTest.Pages
{
    public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
    {
        public Index(Lazy<XncfModuleService> xncfModuleService) : base(xncfModuleService)
        {

        }

        public void OnGet()
        {
        }
    }
}
