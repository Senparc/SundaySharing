﻿using Senparc.Ncf.XncfBase.FunctionRenders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Senparc.Ncf.XncfBase.Functions;

namespace Sunday.Xncf.DemoTest.OHS.Local.PL
{
    public class TestCacheRequest : FunctionAppRequestBase
    {
        [Required]
        [MaxLength(50)]
        [Description("缓存数据||数据将会被缓存")]
        public string Data { get; set; }

        [Description("运算符||")]//下拉列表
        public SelectionList CacheType { get; set; } = new SelectionList(SelectionType.CheckBoxList, new[] {
                 new SelectionItem("local","本地缓存","本地缓存",true),
                 new SelectionItem("redis","Redis缓存","Redis缓存",false)
            });
    }
}
