﻿using Microsoft.EntityFrameworkCore;
using Senparc.CO2NET;
using Senparc.CO2NET.Cache;
using Senparc.CO2NET.Cache.CsRedis;
using Senparc.CO2NET.Extensions;
using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Core.Models;
using Sunday.Xncf.DemoTest.Domain.Services;
using Sunday.Xncf.DemoTest.OHS.Local.PL;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace Sunday.Xncf.DemoTest.OHS.Local.AppService
{
    public class MyFuctionAppService : AppServiceBase
    {
        private ColorService _colorService;
        public MyFuctionAppService(IServiceProvider serviceProvider, ColorService colorService) : base(serviceProvider)
        {
            _colorService = colorService;
        }


        [FunctionRender("缓存测试", "测试缓存读写", typeof(Register))]
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<StringAppResponse> TestCache(TestCacheRequest data)
        {
            return await this.GetResponseAsync<StringAppResponse, string>(async (response, logger) =>
            {
                //选择缓存
                if (data.CacheType.SelectedValues.Contains("local"))
                {
                    var cache = LocalObjectCacheStrategy.Instance;
                    var oldData = await cache.GetAsync<string>("TestCache");

                    logger.Append("当前缓存策略：" + cache.GetType().Name);
                    logger.Append("当前数据：" + oldData);
                    logger.Append("存入缓存：" + data.Data);

                    await cache.SetAsync("TestCache", data.Data);
                }

                if (data.CacheType.SelectedValues.Contains("redis"))
                {
                    var cache = RedisObjectCacheStrategy.Instance;
                    var oldData = await cache.GetAsync<string>("TestCache");

                    logger.Append("当前缓存策略：" + cache.GetType().Name);
                    logger.Append("当前数据：" + oldData);
                    logger.Append("存入缓存：" + data.Data);

                    await cache.SetAsync("TestCache", data.Data);
                }
                

                //var cache = CacheStrategyFactory.GetObjectCacheStrategyInstance();
                

                logger.SaveLogs("TestCache");


                return "储存成功！";
            });
        }


        [FunctionRender("我的函数", "我的函数的注释", typeof(Register))]
        public async Task<StringAppResponse> Calculate(MyFunction_CaculateRequest request)
        {
            return await this.GetResponseAsync<StringAppResponse, string>(async (response, logger) =>
            {
                /* 页面上点击“执行”后，将调用这里的方法
                  *
                  * 参数说明：
                  * response：已经初始化后的返回结果
                  * logger：日志
                  * 
                  * 如果直接对 response 的属性修改，则最终 return null，
                  * 否则可以返回一个新的 response 对象，系统将自动覆盖原有对象
                  */

                double calcResult = request.Number1;
                var theOperator = request.Operator.SelectedValues.FirstOrDefault();
                switch (theOperator)
                {
                    case "+":
                        calcResult = calcResult + request.Number2;
                        break;
                    case "-":
                        calcResult = calcResult - request.Number2;
                        break;
                    case "×":
                        calcResult = calcResult * request.Number2;
                        break;
                    case "÷":
                        if (request.Number2 == 0)
                        {
                            response.Success = false;
                            response.ErrorMessage = "被除数不能为0！";
                            return null;
                        }
                        calcResult = calcResult / request.Number2;
                        break;
                    default:
                        response.Success = false;
                        response.ErrorMessage = $"未知的运算符：{theOperator}";
                        return null;
                }

                logger.Append($"进行运算：{request.Number1} {theOperator} {request.Number2} = {calcResult}");

                Action<int> raisePower = power =>
                {
                    if (request.Power.SelectedValues.Contains(power.ToString()))
                    {
                        var oldValue = calcResult;
                        calcResult = Math.Pow(calcResult, power);
                        logger.Append($"进行{power}次方运算：{oldValue}{(power == 2 ? "²" : "³")} = {calcResult}");
                    }
                };

                raisePower(2);
                raisePower(3);

                response.Data = $"【{request.Name}】计算结果：{calcResult}。计算过程请看日志";
                return null;
            });
        }
    }
}
