﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CO2NETSample
{
    /*
  {
  "weatherinfo": {
    "city": "太仓",
    "cityid": "101190408",
    "temp1": "19℃",
    "temp2": "25℃",
    "weather": "大雨",
    "img1": "n9.gif",
    "img2": "d9.gif",
    "ptime": "18:00"
  }
}
    */

    public class Weather
    {
        public static int Count { get; set; }
        public Weatherinfo weatherinfo { get; set; }
    }

    public class Weatherinfo
    {
        public string city { get; set; }
        public string cityid { get; set; }
        public string temp1 { get; set; }
        public string temp2 { get; set; }
        public string weather { get; set; }
        public string img1 { get; set; }
        public string img2 { get; set; }
        public string ptime { get; set; }
    }

}
