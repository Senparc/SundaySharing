## 说明

前半部分的内容为讲解 [Senparc.Weixin SDK](https://github.com/JeffreySu/WeiXinMPSDK) 和 [CO2NET](https://github.com/Senparc/Senparc.CO2NET) 的缓存策略、领域缓存的设计思路，可以直接查看相关项目源码。

[AsyncDemo](./AsyncDemo) 是后半部分讨论异步编程、线程问题的示例